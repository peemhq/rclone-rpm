%if 0%{?fedora} || 0%{?rhel} == 6
%global with_devel 1
%global with_bundled 1
%global with_debug 0
%global with_check 1
%global with_unit_test 0
%else
%global with_devel 0
%global with_bundled 0
%global with_debug 0
%global with_check 0
%global with_unit_test 0
%endif

%global stable 0

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif


%global provider        github
%global provider_tld    com
%global project         ncw
%global repo            rclone
# https://github.com/ncw/rclone
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     %{provider_prefix}

%global rpm_provider            bitbucket
%global rpm_provider_tld        org    
%global rpm_project             peemhq
%global rpm_repo                rclone-rpm
%global rpm_provider_prefix     %{rpm_provider}.%{rpm_provider_tld}/%{rpm_project}/%{rpm_repo}
%global rpm_import_path         %{rpm_provider_prefix}
%global rpm_buildroot           %{_builddir}/%{name}-%{version}.%{release}.%{_host_cpu}
%global rpm_buildpath           %{rpm_buildroot}/src/%{import_path}


Name:           golang-%{provider}-%{project}-%{repo}
%if 0%{stable} 
Version:        1.34
Release:        1%{?dist}
%else
Version:        1.x
Release:        1%{dist}
%endif
Summary:        rsync for cloud storage
# Detected licences
# - MIT/X11 (BSD like) at 'COPYING'
License:        MIT/X11 (BSD like)
URL:            https://%{provider_prefix}
%if 0%{stable}
Source0:        https://%{rpm_provider_prefix}/get/v%{version}.tar.gz
%else
Source0:        https://%{rpm_provider_prefix}/get/master.tar.gz
%endif

# e.g. el6 has ppc64 arch without gcc-go, so EA tag is required
ExclusiveArch:  %{?go_arches:%{go_arches}}%{!?go_arches:%{ix86} x86_64 %{arm}}
# If go_compiler is not set to 1, there is no virtual provide. Use golang instead.
BuildRequires:  %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}
BuildRequires:  pandoc
BuildRequires:  git



%description
Rclone is a command line program to sync files and directories to and from

    Google Drive
    Amazon S3
    Openstack Swift / Rackspace cloud files / Memset Memstore
    Dropbox
    Google Cloud Storage
    Amazon Drive
    Microsoft One Drive
    Hubic
    Backblaze B2
    Yandex Disk
    The local filesystem

Features

    MD5/SHA1 hashes checked at all times for file integrity
    Timestamps preserved on files
    Partial syncs supported on a whole file basis
    Copy mode to just copy new/changed files
    Sync (one way) mode to make a directory identical
    Check mode to check for file hash equality
    Can sync to and from network, eg two different cloud accounts
    Optional encryption (Crypt)
    Optional FUSE mount


%if 0%{?with_devel}
%package devel
Summary:       %{summary}
BuildArch:     noarch

%if 0%{?with_check} && ! 0%{?with_bundled}
BuildRequires: golang(bazil.org/fuse)
BuildRequires: golang(bazil.org/fuse/fs)
BuildRequires: golang(github.com/Unknwon/goconfig)
BuildRequires: golang(github.com/VividCortex/ewma)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/awserr)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/corehandlers)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/credentials)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/ec2metadata)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/request)
BuildRequires: golang(github.com/aws/aws-sdk-go/aws/session)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/s3)
BuildRequires: golang(github.com/aws/aws-sdk-go/service/s3/s3manager)
BuildRequires: golang(github.com/ncw/go-acd)
BuildRequires: golang(github.com/ncw/swift)
BuildRequires: golang(github.com/ogier/pflag)
BuildRequires: golang(github.com/pkg/errors)
BuildRequires: golang(github.com/rfjakob/eme)
BuildRequires: golang(github.com/skratchdot/open-golang/open)
BuildRequires: golang(github.com/spf13/cobra)
BuildRequires: golang(github.com/spf13/cobra/doc)
BuildRequires: golang(github.com/spf13/pflag)
BuildRequires: golang(github.com/stacktic/dropbox)
BuildRequires: golang(github.com/stretchr/testify/assert)
BuildRequires: golang(github.com/stretchr/testify/require)
BuildRequires: golang(github.com/tsenart/tb)
BuildRequires: golang(golang.org/x/crypto/nacl/secretbox)
BuildRequires: golang(golang.org/x/crypto/scrypt)
BuildRequires: golang(golang.org/x/crypto/ssh/terminal)
BuildRequires: golang(golang.org/x/net/context)
BuildRequires: golang(golang.org/x/oauth2)
BuildRequires: golang(golang.org/x/oauth2/google)
BuildRequires: golang(golang.org/x/sys/unix)
BuildRequires: golang(golang.org/x/text/unicode/norm)
BuildRequires: golang(google.golang.org/api/drive/v2)
BuildRequires: golang(google.golang.org/api/googleapi)
BuildRequires: golang(google.golang.org/api/storage/v1)
BuildRequires: golang(github.com/mreiferson/go-httpclient)
%endif

Requires:      golang(bazil.org/fuse)
Requires:      golang(bazil.org/fuse/fs)
Requires:      golang(github.com/Unknwon/goconfig)
Requires:      golang(github.com/VividCortex/ewma)
Requires:      golang(github.com/aws/aws-sdk-go/aws)
Requires:      golang(github.com/aws/aws-sdk-go/aws/awserr)
Requires:      golang(github.com/aws/aws-sdk-go/aws/corehandlers)
Requires:      golang(github.com/aws/aws-sdk-go/aws/credentials)
Requires:      golang(github.com/aws/aws-sdk-go/aws/credentials/ec2rolecreds)
Requires:      golang(github.com/aws/aws-sdk-go/aws/ec2metadata)
Requires:      golang(github.com/aws/aws-sdk-go/aws/request)
Requires:      golang(github.com/aws/aws-sdk-go/aws/session)
Requires:      golang(github.com/aws/aws-sdk-go/service/s3)
Requires:      golang(github.com/aws/aws-sdk-go/service/s3/s3manager)
Requires:      golang(github.com/ncw/go-acd)
Requires:      golang(github.com/ncw/swift)
Requires:      golang(github.com/ogier/pflag)
Requires:      golang(github.com/pkg/errors)
Requires:      golang(github.com/rfjakob/eme)
Requires:      golang(github.com/skratchdot/open-golang/open)
Requires:      golang(github.com/spf13/cobra)
Requires:      golang(github.com/spf13/cobra/doc)
Requires:      golang(github.com/spf13/pflag)
Requires:      golang(github.com/stacktic/dropbox)
Requires:      golang(github.com/stretchr/testify/assert)
Requires:      golang(github.com/stretchr/testify/require)
Requires:      golang(github.com/tsenart/tb)
Requires:      golang(golang.org/x/crypto/nacl/secretbox)
Requires:      golang(golang.org/x/crypto/scrypt)
Requires:      golang(golang.org/x/crypto/ssh/terminal)
Requires:      golang(golang.org/x/net/context)
Requires:      golang(golang.org/x/oauth2)
Requires:      golang(golang.org/x/oauth2/google)
Requires:      golang(golang.org/x/sys/unix)
Requires:      golang(golang.org/x/text/unicode/norm)
Requires:      golang(google.golang.org/api/drive/v2)
Requires:      golang(google.golang.org/api/googleapi)
Requires:      golang(google.golang.org/api/storage/v1)

Provides:      golang(%{import_path}/amazonclouddrive) = %{version}-%{release}
Provides:      golang(%{import_path}/b2) = %{version}-%{release}
Provides:      golang(%{import_path}/b2/api) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/all) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/authorize) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/cat) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/check) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/cleanup) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/config) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/copy) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/dedupe) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/delete) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/genautocomplete) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/gendocs) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/listremotes) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/ls) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/lsd) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/lsl) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/md5sum) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/memtest) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/mkdir) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/mount) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/move) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/purge) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/rmdir) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/sha1sum) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/size) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/sync) = %{version}-%{release}
Provides:      golang(%{import_path}/cmd/version) = %{version}-%{release}
Provides:      golang(%{import_path}/crypt) = %{version}-%{release}
Provides:      golang(%{import_path}/crypt/pkcs7) = %{version}-%{release}
Provides:      golang(%{import_path}/dircache) = %{version}-%{release}
Provides:      golang(%{import_path}/drive) = %{version}-%{release}
Provides:      golang(%{import_path}/dropbox) = %{version}-%{release}
Provides:      golang(%{import_path}/fs) = %{version}-%{release}
Provides:      golang(%{import_path}/fs/all) = %{version}-%{release}
Provides:      golang(%{import_path}/fstest) = %{version}-%{release}
Provides:      golang(%{import_path}/fstest/fstests) = %{version}-%{release}
Provides:      golang(%{import_path}/googlecloudstorage) = %{version}-%{release}
Provides:      golang(%{import_path}/hubic) = %{version}-%{release}
Provides:      golang(%{import_path}/local) = %{version}-%{release}
Provides:      golang(%{import_path}/oauthutil) = %{version}-%{release}
Provides:      golang(%{import_path}/onedrive) = %{version}-%{release}
Provides:      golang(%{import_path}/onedrive/api) = %{version}-%{release}
Provides:      golang(%{import_path}/pacer) = %{version}-%{release}
Provides:      golang(%{import_path}/rest) = %{version}-%{release}
Provides:      golang(%{import_path}/s3) = %{version}-%{release}
Provides:      golang(%{import_path}/swift) = %{version}-%{release}
Provides:      golang(%{import_path}/yandex) = %{version}-%{release}
Provides:      golang(%{import_path}/yandex/api) = %{version}-%{release}

%description devel
This package contains library source for rclone 
intended for building other packages which 
use import path with %{import_path} prefix.
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%package unit-test-devel
Summary:         Unit tests for %{name} package
%if 0%{?with_check}
#Here comes all BuildRequires: PACKAGE the unit tests
#in %%check section need for running
%endif

# test subpackage tests code from devel subpackage
Requires:        %{name}-devel = %{version}-%{release}

%if 0%{?with_check} && ! 0%{?with_bundled}
%endif


%description unit-test-devel
This package contains unit tests for rclone
providing packages with %{import_path} prefix.
%endif

%prep
git clone --recursive https://%{rpm_provider_prefix} %{rpm_buildpath}

%build
cd %{rpm_buildpath}

export GOPATH='%{rpm_buildroot}'
go build -v rclone.go
pandoc --from markdown_github --to html --standalone README.md --output README.html 
pandoc --from markdown_github --to html --standalone MANUAL.md --output MANUAL.html  
pandoc --from markdown_github --to html --standalone RELEASE.md --output RELEASE.html
pandoc --from markdown_github --to plain --standalone README.md --output README.txt
pandoc --from markdown_github --to plain --standalone MANUAL.md --output MANUAL.txt
pandoc --from markdown_github --to plain --standalone RELEASE.md --output RELEASE.txt

%install
mkdir -p %{buildroot}%{_mandir}/man1
mkdir -p %{buildroot}%{_bindir}
mkdir -p %{buildroot}%{_docdir}/rclone
install %{rpm_buildroot}src/%{import_path}/rclone.1 %{buildroot}%{_mandir}/man1/
install %{rpm_buildroot}src/%{import_path}/rclone %{buildroot}%{_bindir}/
install %{rpm_buildroot}src/%{import_path}/README.* %{buildroot}%{_docdir}/rclone
install %{rpm_buildroot}src/%{import_path}/MANUAL.* %{buildroot}%{_docdir}/rclone
install %{rpm_buildroot}src/%{import_path}/RELEASE.* %{buildroot}%{_docdir}/rclone
install %{rpm_buildroot}src/%{import_path}/COPYING %{buildroot}%{_docdir}/rclone
rm -f %{rpm_buildroot}/%{_docdir}/rclone/*.md 

# source codes for building projects
%if 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
echo "%%dir %%{gopath}/src/%%{import_path}/." >> devel.file-list
# find all *.go but no *_test.go files and generate devel.file-list
for file in $(find . \( -iname "*.go" -or -iname "*.s" \) \! -iname "*_test.go") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

# testing files for this project
%if 0%{?with_unit_test} && 0%{?with_devel}
install -d -p %{buildroot}/%{gopath}/src/%{import_path}/
# find all *_test.go files and generate unit-test-devel.file-list
for file in $(find . -iname "*_test.go") ; do
    dirprefix=$(dirname $file)
    install -d -p %{buildroot}/%{gopath}/src/%{import_path}/$dirprefix
    cp -pav $file %{buildroot}/%{gopath}/src/%{import_path}/$file
    echo "%%{gopath}/src/%%{import_path}/$file" >> unit-test-devel.file-list

    while [ "$dirprefix" != "." ]; do
        echo "%%dir %%{gopath}/src/%%{import_path}/$dirprefix" >> devel.file-list
        dirprefix=$(dirname $dirprefix)
    done
done
%endif

%if 0%{?with_devel}
sort -u -o devel.file-list devel.file-list
%endif

%check
%if 0%{?with_check} && 0%{?with_unit_test} && 0%{?with_devel}
%if ! 0%{?with_bundled}
export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%else
# No dependency directories so far

export GOPATH=%{buildroot}/%{gopath}:%{gopath}
%endif

%if ! 0%{?gotest:1}
%global gotest go test
%endif

%gotest %{import_path}/amazonclouddrive
%gotest %{import_path}/b2
%gotest %{import_path}/b2/api
%gotest %{import_path}/cmd
%gotest %{import_path}/cmd/mount
%gotest %{import_path}/crypt
%gotest %{import_path}/crypt/pkcs7
%gotest %{import_path}/drive
%gotest %{import_path}/dropbox
%gotest %{import_path}/fs
%gotest %{import_path}/googlecloudstorage
%gotest %{import_path}/hubic
%gotest %{import_path}/local
%gotest %{import_path}/onedrive
%gotest %{import_path}/pacer
%gotest %{import_path}/s3
%gotest %{import_path}/swift
%gotest %{import_path}/yandex
%endif

#define license tag if not already defined
%{!?_licensedir:%global license %doc}


%if 0%{?with_devel}
%files devel -f devel.file-list
%license COPYING
%doc CONTRIBUTING.md ISSUE_TEMPLATE.md MANUAL.md RELEASE.md README.md
%dir %{gopath}/src/%{provider}.%{provider_tld}/%{project}
%endif

%if 0%{?with_unit_test} && 0%{?with_devel}
%files unit-test-devel -f unit-test-devel.file-list
%license COPYING
%doc CONTRIBUTING.md ISSUE_TEMPLATE.md MANUAL.md RELEASE.md README.md
%endif

%files
%{_mandir}
%{_bindir}
%{_docdir}

%changelog
* Thu Oct 27 2016 Mockbuild user for kde4-fedora repo builds <peemhq@gmail.com> - 0-0.1.git8710741
- First package for Fedora

